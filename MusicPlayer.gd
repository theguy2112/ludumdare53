extends Node2D

enum State{
	Nothing,
	Base,
	Action,
	GameOver
}

var _current_state = State.Nothing : set = _set_state

func play_menu():
	_current_state = State.Base

func play_action():
	_current_state = State.Action

func play_game_over():
	_current_state = State.GameOver

func play_other_menu():
	_current_state = State.Base

func play_map():
	_current_state = State.Base


func _set_state(new_state):
	if _current_state == new_state:
		return
	_current_state = new_state
	if new_state == State.Base:
		$BassLineMusic.stop()
		$GameOverMusic.stop()
		$BaseMusic.play()
	elif new_state == State.Action:
		$BassLineMusic.play()
		$BassLineMusic.seek($BaseMusic.get_playback_position())
	elif new_state == State.GameOver:
		$BaseMusic.stop()
		$BassLineMusic.stop()
		$GameOverMusic.play()
