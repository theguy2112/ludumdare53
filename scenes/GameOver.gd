extends Control

func _ready():
	MusicPlayer.play_game_over()
	$AnimationPlayer.play("idle")
	$TextureRect/MarginContainer/CenterContainer/VBoxContainer/HBoxContainer2/ScoreLabel.text = str(int(Progress.total_score))
	$TextureRect/MarginContainer/CenterContainer/VBoxContainer/HBoxContainer3/DistanceLabel.text = str(int(Progress.total_travelled_distance)) + " km"
	$TextureRect/MarginContainer/CenterContainer/VBoxContainer/HBoxContainer4/DaysLabel.text = str(Progress.days_passed)

func _process(delta):
	var ship1 = $TextureRect/Ship
	var ship2 = $TextureRect/Ship2
	
	ship1.global_position.x += delta * 120
	ship1.global_position.y += delta*20
	ship2.global_position.x += delta * 120
	ship2.global_position.y -= delta*20
	
	if ship1.global_position.x > 1000:
		ship1.global_position.x = -200
		ship1.global_position.y = randi_range(100,300)

	if ship2.global_position.x > 1000:
		ship2.global_position.x = -200
		ship2.global_position.y = randi_range(100,300)
	
	


func _on_button_pressed():
	UiSounds.play_click_sound()
	SceneTransition.transition_to_file(self, "res://scenes/StartMenu.tscn")


func _on_button_mouse_entered():
	UiSounds.play_hover_sound()
