extends Control

signal continued

var puked = false
var died = false
var score: int = 0
var route: Route = Route.new(0,3,50,true)

@onready
var titleLabel = $TextureRect/MarginContainer/CenterContainer/VBoxContainer/Title
@onready
var scoreLabel = $TextureRect/MarginContainer/CenterContainer/VBoxContainer/HBoxContainer2/ScoreLabel
@onready
var button = $TextureRect/MarginContainer/CenterContainer/VBoxContainer/Button

func _ready():
	MusicPlayer.play_other_menu()
	titleLabel.text = "Finished route successfully!"
	if puked:
		titleLabel.text = "You made Deli puke!\nNext time fly more gentle..."
	if died:
		titleLabel.text = "Deli and you were captured\nby space pirates!"
		button.text = "Game over!"

	scoreLabel.text = str(score)
	
	$PlanetFrom.seed = route.from_planet_seed
	$PlanetTo.seed = route.to_planet_seed


func _on_button_pressed():
	UiSounds.play_click_sound()
	continued.emit()


func _on_button_mouse_entered():
	UiSounds.play_hover_sound()
