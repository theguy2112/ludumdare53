extends Node2D


func _ready():
	var tween = get_tree().create_tween()
	tween.tween_property($CanvasLayer/Container, "modulate", Color(1,1,1,1), 0.5)
	tween.play()
	$AudioStreamPlayer.play()


func _on_timer_timeout():
	SceneTransition.transition_to_file(self, "res://scenes/StartMenu.tscn")
