extends Node2D

var Planet = preload("res://scenes/map/Planet.tscn")
var Connection = preload("res://scenes/map/Connection.tscn")
var RouteHandler = preload("res://scenes/action/RouteHandler.tscn")

var _current_selected_planet = null

var _connection_nodes = {}
var _connections = []

var planets_to_generate = 7
var home_planet = null
var routes_to_plan = 3

@onready
var routesLabel = $CanvasLayer/Control/MarginContainer/HBoxContainer/RoutesLabel
@onready
var startButton = $CanvasLayer/Control/MarginContainer2/StartButton
@onready
var connectionsNodeParent = $Connections

func _ready():
	MusicPlayer.play_map()
	$Background.material.get_shader_parameter("backgroundTexture").noise.seed = randi()
	$Background.material.get_shader_parameter("starTexture").noise.seed = randi()
	
	for child in $Planets.get_children():
		$Planets.remove_child(child)
	
	if Progress.days_passed == 0:
		routes_to_plan = 1
		planets_to_generate = 0
		generate_planet(3)
		generate_planet(0)
		var planets = $Planets.get_children()
		planets[0].global_position = Vector2(300, 200)
		planets[1].global_position = Vector2(500, 200)
		home_planet = planets[0]
		# TODO show that you need to connect the home planet to other planets
	elif Progress.days_passed == 1:
		routes_to_plan = 2
		planets_to_generate = 0
		generate_planet(4)
		generate_planet(1)
		var planets = $Planets.get_children()
		planets[0].global_position = Vector2(300, 200)
		planets[1].global_position = Vector2(500, 200)
		home_planet = planets[0]
		# TODO show that routes can between planets twice
	elif Progress.days_passed == 2:
		routes_to_plan = 3
		planets_to_generate = 3
		# TODO show: "now you know how to play" message
	elif Progress.days_passed == 3:
		routes_to_plan = 3
		planets_to_generate = 4
	else:
		planets_to_generate = randi_range(4,7)
		routes_to_plan = randi_range(-1,2) + planets_to_generate
	
	if planets_to_generate > 0:
		for i in range(planets_to_generate):
			generate_planet()
		home_planet = $Planets.get_children().pick_random()
	$PlanetHome.position = home_planet.position
	
	for planet in $Planets.get_children():
		planet.selected.connect(self._planet_selected.bind(planet))
		planet.unselected.connect(self._planet_unselected.bind(planet))
		planet.hovered.connect(self._planet_hovered.bind(planet))
		planet.unhovered.connect(self._planet_unhovered.bind(planet))
	
	routesLabel.text = str(routes_to_plan)
		
	$CanvasLayer/Control/MarginContainer3/HBoxContainer/DayLabel.text = "Day "+str(Progress.days_passed)
	$CanvasLayer/Control/MarginContainer3/HBoxContainer/ScoreLabel.text = "Score: "+str(int(Progress.total_score))

func generate_planet(seed = null):
	var rect = $PlanetGenerationRect.shape.get_rect()
	var base_position = $PlanetGenerationRect.position
	var random_x = randf_range(rect.position.x, rect.end.x)
	var random_y = randf_range(rect.position.y, rect.end.y)
	var random_position = Vector2(random_x, random_y)+base_position

	while distance_to_closest_planet(random_position) < 120:
		random_x = randf_range(rect.position.x, rect.end.x)
		random_y = randf_range(rect.position.y, rect.end.y)
		random_position = Vector2(random_x, random_y)+base_position
	
	var planet = Planet.instantiate()
	planet.position = random_position
	if seed != null:
		planet.seed = seed
	$Planets.add_child(planet)


func distance_to_closest_planet(target: Vector2):
	var smallest_distance = 10000000
	for planet in $Planets.get_children():
		smallest_distance = min(smallest_distance, planet.position.distance_to(target))
	return smallest_distance


func _planet_selected(planet):
	if _current_selected_planet == null:
		$PlanetSelector.position = planet.position
		$PlanetSelector.visible = true
		_current_selected_planet = planet
	else:
		_connect_selected_planet_to(planet)
		_current_selected_planet = null


func _connect_selected_planet_to(planet):
	var connection_pair = [_current_selected_planet, planet]
	if not connection_pair in _connection_nodes:
		if available_routes() > 0:
			if _new_route_is_connected_to_home(_current_selected_planet):
				var connection = Connection.instantiate()
				connection.from_planet = _current_selected_planet
				connection.to_planet = planet
				connectionsNodeParent.add_child(connection)
				_connections.append(connection_pair)
				_connection_nodes[connection_pair] = connection
	else:
		var connection_node = _connection_nodes[connection_pair]
		connectionsNodeParent.remove_child(connection_node)
		_connection_nodes.erase(connection_pair)
		_connections.erase(connection_pair)
	routesLabel.text = str(available_routes())
	
	if available_routes() == 0:
		startButton.visible = true
	else:
		startButton.visible = false
	
	_current_selected_planet.unselect()
	planet.unselect()
	_planet_unselected(_current_selected_planet)


func available_routes() -> int:
	return routes_to_plan - len(_connections)


func _planet_unselected(planet):
	$PlanetSelector.visible = false
	_current_selected_planet = null


func _planet_hovered(planet):
	$PlanetHover.position = planet.position
	$PlanetHover.visible = true
	

func _planet_unhovered(planet):
	$PlanetHover.visible = false


func generate_routes():
	var routes = []
	for connection in _connections:
		var distance = connection[1].position.distance_to(connection[0].position)
		var route = Route.new(connection[0].seed, connection[1].seed, distance/5, connection[0].has_cargo)
		routes.append(route)
	return routes


func _on_start_button_pressed():
	var routes = generate_routes()
	var routeHandler = RouteHandler.instantiate()
	routeHandler.set_routes(routes)
	SceneTransition.transition_to_node(self, routeHandler)


func _new_route_is_connected_to_home(from_planet) -> bool:
	if from_planet == home_planet:
		return true
	for connection in _connections:
		if connection[1] == from_planet:
			return true
	return false
