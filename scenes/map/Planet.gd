extends Node2D

signal selected
signal unselected
signal hovered
signal unhovered

var _is_selected = false
var _name = ""
var seed: int = randi() : 
	set(new_seed):
		seed = new_seed
		if sprite != null:
			_initialize_from_seed()

var planetTextures = [
	"res://assets/sprites/planets/planet1.png",
	"res://assets/sprites/planets/planet2.png",
	"res://assets/sprites/planets/planet3.png",
	"res://assets/sprites/planets/planet4.png",
]

@export
var showName: bool = true

@onready
var sprite = $Sprite
@onready
var nameLabel = $Control/CenterContainer/NameLabel
@onready
var packagePivot = $PackagePivot
@onready
var package = $PackagePivot/Package

var package_rotation_speed: float
var package_orbit_speed_multiplier: float
var has_cargo: bool

func _ready():
	_initialize_from_seed()
	$PackagePivot.visible = has_cargo


func _initialize_from_seed():
	var rng = RandomNumberGenerator.new()
	rng.seed = seed

	var randomPlanetTexture = load(planetTextures[rng.randi_range(0, len(planetTextures)-1)])
	sprite.material.set_shader_parameter("planetTexture", randomPlanetTexture)
	
	var backgroundColor = ColorPalette.get_random_planet_color(rng)
	sprite.material.set_shader_parameter("backgroundColor", backgroundColor)
	
	var hightlightColor1 = ColorPalette.get_random_planet_color(rng)
	sprite.material.set_shader_parameter("highlightColor1", hightlightColor1)
	
	var hightlightColor2 = ColorPalette.get_random_planet_color(rng)
	sprite.material.set_shader_parameter("highlightColor2", hightlightColor2)
	
	
	$AnimationPlayer.playback_speed = rng.randf_range(0.9,1.1)/4
	$AnimationPlayer.play("idle")
	
	_name = _get_random_name(rng)
	nameLabel.text = _name
	
	if not showName:
		nameLabel.visible = false
		
	has_cargo = rng.randf() > 0.5
	
	packagePivot.visible = has_cargo
	package_orbit_speed_multiplier = rng.randf_range(0.9,1.1)
	package_rotation_speed = rng.randf_range(-PI/4, PI/4)
	
	if rng.randf() > 0.5:
		package_orbit_speed_multiplier *= -1

func _get_random_name(rng: RandomNumberGenerator):
	var syllables = [
		"an",
		"be",
		"rok",
		"bor",
		"nev",
		"kir",
		"qwo",
		"al",
		"per",
		"kho",
		"sh",
		"pri",
		"nox",
		"mis",
		"stri"
	]
	var amount = rng.randi_range(2,3)
	var name = ""
	for i in range(amount):
		name += syllables[rng.randi_range(0, len(syllables)-1)]
	return name.substr(0, 1).to_upper() + name.substr(1)


func _on_area_2d_mouse_entered():
	UiSounds.play_hover_sound()
	emit_signal("hovered")


func _on_area_2d_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.pressed:
		UiSounds.play_click_sound()
		if _is_selected:
			_is_selected = false
			emit_signal("unselected")
		else:
			_is_selected = true
			emit_signal("unhovered")
			emit_signal("selected")


func _on_area_2d_mouse_exited():
	emit_signal("unhovered")


func unselect():
	_is_selected = false


func _physics_process(delta):
	packagePivot.rotation += delta*package_orbit_speed_multiplier
	package.rotation += delta*package_rotation_speed
