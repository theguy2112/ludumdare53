extends Line2D

@export
var to_planet: Node2D = null

@export
var from_planet: Node2D = null

func _ready():
	compute_new_line()

func compute_new_line():
	if from_planet == null or to_planet == null:
		return

	clear_points()
	
	var point1 = from_planet.position
	
	var vec_between = to_planet.position - from_planet.position
	var vec_to_bent = vec_between/2 + (vec_between/3).rotated(-PI/2)
	var point2 = from_planet.position+vec_to_bent
	
	var point3 = to_planet.position
	
	var amount_points = 3
	
	add_point(from_planet.position)
	
	for i in range(amount_points):
		var value = i/float(amount_points)
		var q0 = lerp(point1, point2, value)
		var q1 = lerp(point2, point3, value)
		var vec = lerp(q0, q1, value)
		add_point(vec)
	
	add_point(to_planet.position)
