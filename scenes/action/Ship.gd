extends CharacterBody2D

signal hit
signal puked
signal pan_camera(direction)

const SPEED = 250.0

var has_cargo: bool = true : 
	set(new_has_cargo):
		has_cargo = new_has_cargo
		if $Cargo != null:
			$Cargo.visible = new_has_cargo

var Explosion = preload("res://scenes/action/ExplosionBlue.tscn")
var lastMovement = Vector2.ZERO

var sickness: float = 0

func _ready():
	$AnimationPlayer.play("idle")
	$Cargo.visible = has_cargo

func _physics_process(delta):
	sickness = max(0, sickness-delta/5)
	
	var direction = Vector2(
		Input.get_action_strength("right") - Input.get_action_strength("left"),
		Input.get_action_strength("down") - Input.get_action_strength("up")
	)
	var old_direction = velocity / SPEED
	velocity = direction * SPEED
	move_and_slide()

	if direction.y > 0:
		rotation_degrees = 8
	elif direction.y < 0:
		rotation_degrees = -8
	else:
		rotation = 0
	
	if old_direction != direction:
		pan_camera.emit(direction)
		if direction == Vector2.ZERO:
			pass
		elif old_direction == Vector2.ZERO:
			sickness += 0.3
		else:
			sickness += 0.5
		if sickness > 1:
			puked.emit()


func get_hit():
	$HitAudioPlayer.play()
	var explosion = Explosion.instantiate()
	explosion.emitting = true
	add_child(explosion)
	hit.emit()


func _on_hitbox_area_entered(area):
		get_hit()
