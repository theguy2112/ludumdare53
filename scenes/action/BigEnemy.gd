extends Area2D

@export
var speed = 150
@export
var direction = Vector2.LEFT

var Bullet = preload("res://scenes/action/EnemyBullet.tscn")

@onready
var pathFollow = $BulletSpawnPath/PathFollow2D

var amountBullets = randi_range(10,13)

func _ready():
	global_position+=Vector2(500,200)
	var random_shoot_rate = randf_range(1.5,2.5)
	$ShootTimer.stop()
	$ShootTimer.wait_time = random_shoot_rate
	$ShootTimer.start()
	$AnimationPlayer.play("idle")
	shoot()

func _physics_process(delta):
	translate(direction * speed * delta)


func _on_area_entered(area):
	destroy()


func destroy():
	queue_free()


func _on_shoot_timer_timeout():
	shoot()


func shoot():
	for i in range(amountBullets):
		var p = float(i)/float(amountBullets)
		pathFollow.progress_ratio = p
		var vec_from_center = pathFollow.global_position - global_position
		var bullet = Bullet.instantiate()
		bullet.global_position = pathFollow.global_position
		bullet.direction = vec_from_center.normalized()
		bullet.look_at(pathFollow.global_position+vec_from_center)
		bullet.speed /= 2
		bullet.color = ColorPalette.colors[18]
		get_parent().add_child(bullet)
	$AudioStreamPlayer.play()
