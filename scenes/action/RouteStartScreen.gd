extends Control

signal closed

@onready
var distanceLabel = $TextureRect/MarginContainer/CenterContainer/VBoxContainer/HBoxContainer/DistanceLabel

var route: Route = null

func _ready():
	MusicPlayer.play_other_menu()
	$PlanetFrom.seed = route.from_planet_seed
	$PlanetTo.seed = route.to_planet_seed
	distanceLabel.text = str(int(route.distance)) + " km"


func _on_button_pressed():
	UiSounds.play_click_sound()
	closed.emit()


func _on_button_mouse_entered():
	UiSounds.play_hover_sound()
