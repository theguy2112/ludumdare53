extends Node2D

signal finished(rest_hp)
signal died(rest_hp, passed_distance)
signal puked(rest_hp, passed_distance)

var Asteroid = preload("res://scenes/action/Asteroid.tscn")
var Enemy = preload("res://scenes/action/Enemy.tscn")
var BigEnemy = preload("res://scenes/action/BigEnemy.tscn")

@onready
var meterLabel = $CanvasLayer/Control/MarginContainer2/MeterLabel
@onready
var asteroidTimer = $AsteroidTimer
@onready
var enemyTimer = $EnemyTimer
@onready
var metersToFlyTimer = $MetersToFlyTimer
@onready
var healthTexture = $CanvasLayer/Control/MarginContainer/HBoxContainer/HealthTexture
@onready
var distanceIndicator = $CanvasLayer/DistanceIndicator
@onready
var planetFrom = $CanvasLayer/PlanetFrom
@onready
var planetTo = $CanvasLayer/PlanetTo
@onready
var ship = $Ship
@onready
var sicknessIndicator = $CanvasLayer/Control/MarginContainer/HBoxContainer/SicknessBar/SicknessIndicator
@onready
var spawnPathFollow = $SpawnPath/PathFollow2D
@onready
var bigEnemyTimer = $BigEnemyTimer

var health = 0: set = set_health

var route: Route = Route.new(0,1,50,true)

var background_direction = Vector2(1,0)
var background_position = Vector2(0,0)

func _ready():
	MusicPlayer.play_action()
	$Background.material.get_shader_parameter("backgroundTexture").noise.seed = randi()
	$Background.material.get_shader_parameter("starTexture").noise.seed = randi()
	
	planetFrom.seed = route.from_planet_seed
	planetTo.seed = route.to_planet_seed
	
	metersToFlyTimer.wait_time = route.distance
	
	if Progress.days_passed > 10:
		enemyTimer.playback_speed = 2
		asteroidTimer.playback_speed = 2
		bigEnemyTimer.playback_speed = 2
	elif Progress.days_passed > 5:
		enemyTimer.playback_speed = 1.5
		asteroidTimer.playback_speed = 1.5
		bigEnemyTimer.playback_speed = 1.5
	elif Progress.days_passed > 3:
		enemyTimer.playback_speed = 1.2
		asteroidTimer.playback_speed = 1.2
		bigEnemyTimer.playback_speed = 1.2
	
	$Ship.has_cargo = route.has_cargo
	
	set_health(health)
	start()

func _on_asteroid_timer_timeout():
	var asteroid = Asteroid.instantiate()
	spawnPathFollow.progress_ratio = randf()
	var randomPosition = spawnPathFollow.global_position
	asteroid.global_position = randomPosition
	add_child(asteroid)


func _on_enemy_timer_timeout():
	var enemy = Enemy.instantiate()
	spawnPathFollow.progress_ratio = randf()
	var randomPosition = spawnPathFollow.global_position
	enemy.global_position = randomPosition
	add_child(enemy)


func start():
	metersToFlyTimer.start()
	enemyTimer.start()
	bigEnemyTimer.start()
	asteroidTimer.start()


func _on_meters_to_fly_timeout():
	stop()
	finished.emit(health)


func _physics_process(delta):
	var timeLeft = int(metersToFlyTimer.time_left)
	meterLabel.text = str(timeLeft)+" km"
	_set_distance_indicator()
	_set_sickness_level()
	background_position += background_direction * delta / 2
	$Background.material.set_shader_parameter("position", background_position)


func _set_distance_indicator():
	var min_x = 81
	var max_x = 271
	var distance_percent = metersToFlyTimer.time_left /  metersToFlyTimer.wait_time
	var x = lerp(min_x, max_x, 1-distance_percent)
	distanceIndicator.position.x = x


func _set_sickness_level():
	var min_x = 0
	var max_x = 188
	var sickness = ship.sickness
	sicknessIndicator.size.x = lerp(min_x, max_x, sickness)

func stop():
	metersToFlyTimer.paused = true
	enemyTimer.paused = true
	asteroidTimer.paused = true
	bigEnemyTimer.paused = true


func set_health(new_hp: int):
	health = new_hp
	if healthTexture != null:
		healthTexture.custom_minimum_size.x = health * 32
	
	if health == 0:
		died.emit(health, _passed_distance())
		stop()
		healthTexture.visible = false

func _on_ship_hit():
	$AnimationPlayer.play("camera_shake")
	set_health(health-1)


func _on_ship_puked():
	puked.emit(health, _passed_distance())
	stop()


func _passed_distance():
	return metersToFlyTimer.wait_time-metersToFlyTimer.time_left


func _on_big_enemy_timer_timeout():
	var enemy = BigEnemy.instantiate()
	spawnPathFollow.progress_ratio = randf()
	var randomPosition = spawnPathFollow.global_position
	enemy.global_position = randomPosition
	add_child(enemy)


func _on_ship_pan_camera(direction):
	background_direction = Vector2(1,0)+direction*0.1
