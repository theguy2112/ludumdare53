extends Node2D

signal finished

@onready
var StartButton = $CanvasLayer/Control/ColorRect/MarginContainer/StartButton


var _dropped_packages = 0:
	set(new_dropped_packages):
		_dropped_packages = new_dropped_packages
		for package in $CanvasLayer/ShowPackages.get_children().slice(0,new_dropped_packages):
			package.visible = true
		if new_dropped_packages == 3:
			StartButton.visible = true


func _on_package_dropped():
	_dropped_packages += 1
	UiSounds.play_click_sound()


func _on_start_button_pressed():
	finished.emit()
