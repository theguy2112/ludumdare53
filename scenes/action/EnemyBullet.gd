extends Area2D

@export
var speed = 500
var direction = Vector2.LEFT
var color = ColorPalette.colors[19]

func _ready():
	$Line2D.default_color = color
	

func _physics_process(delta):
	translate(direction * speed * delta)


func destroy():
	queue_free()


func _on_area_entered(area):
	destroy()
