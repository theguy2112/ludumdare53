extends Area2D

signal dropped

var _initial_position
var _picked_up = false

func _ready():
	_initial_position = global_position

func _on_area_entered(area):
	dropped.emit()
	queue_free()


func _on_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed:
			_picked_up = true
			UiSounds.play_hover_sound()
		else:
			_picked_up = false
			global_position = _initial_position
		

func _physics_process(delta):
	if _picked_up:
		global_position = get_global_mouse_position()


func _on_package_dropped():
	dropped.emit()
	queue_free()
