extends Area2D

var direction = Vector2.LEFT.rotated(randf_range(-PI/4, PI/4))

@export
var speed = 500

var rotation_speed = 0

func _ready():
	var random_scale = randf_range(1,2)
	scale = Vector2(random_scale, random_scale)
	speed = speed * (2.5-random_scale)
	
	if randf() > 0.5:
		rotation_speed = randf_range(PI/6, PI/4)
	else:
		rotation_speed = randf_range(-PI/4, -PI/6)

func _physics_process(delta):
	translate(direction * speed * delta)
	rotate(rotation_speed*delta)


func _on_area_entered(area):
	destroy()


func destroy():
	queue_free()
