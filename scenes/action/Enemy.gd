extends Area2D

@export
var speed = 300
@export
var direction = Vector2.LEFT

var Bullet = preload("res://scenes/action/EnemyBullet.tscn")

func _ready():
	var random_shoot_rate = randf_range(0.3,1)
	$ShootTimer.stop()
	$ShootTimer.wait_time = random_shoot_rate
	$ShootTimer.start()
	$AnimationPlayer.play("idle")
	shoot()

func _physics_process(delta):
	translate(direction * speed * delta)


func _on_area_entered(area):
	destroy()


func destroy():
	queue_free()


func _on_shoot_timer_timeout():
	shoot()


func shoot():
	var bullet = Bullet.instantiate()
	bullet.global_position = global_position+Vector2(-45,0)
	get_parent().add_child(bullet)
	$AudioStreamPlayer.play()
