extends Control

var MainScene = preload("res://scenes/action/Main.tscn")
var RouteStartScreen = preload("res://scenes/action/RouteStartScreen.tscn")
var RouteSummary = preload("res://scenes/RouteSummary.tscn")
var PackageLoading = preload("res://scenes/action/PackageLoading.tscn")

var _routes_to_fly = []

var current_route = null
var current_hp = 3

func _ready():
	if len(_routes_to_fly) > 0:
		loadNextRoute()


func set_routes(routes):
	_routes_to_fly = routes


func loadNextRoute():
	if len(_routes_to_fly) <= 0:
		return

	current_route = _routes_to_fly.pop_front()
	
	var startScreen = RouteStartScreen.instantiate()
	startScreen.route = current_route
	
	startScreen.closed.connect(_start_screen_closed.bind(startScreen))
	add_child(startScreen)


func _start_screen_closed(startScreen):
	if current_route.has_cargo:
		var packageLoadingScene = PackageLoading.instantiate()
		packageLoadingScene.finished.connect(_package_loading_finished.bind(packageLoadingScene))
		
		SceneTransition.transition_to_node(startScreen, packageLoadingScene)
	else:
		_package_loading_finished(startScreen)
	

func _package_loading_finished(packageLoadingScreen):
	var actionScreen = MainScene.instantiate()
	actionScreen.route = current_route
	actionScreen.health = current_hp
	
	actionScreen.finished.connect(_finished_route.bind(actionScreen))
	actionScreen.died.connect(_died_on_route.bind(actionScreen))
	actionScreen.puked.connect(_puked_on_route.bind(actionScreen))
	
	SceneTransition.transition_to_node(packageLoadingScreen, actionScreen)


func _finished_route(rest_hp: int, actionScreen):
	current_hp = rest_hp
	Progress.total_score += current_route.score_when_finished()
	Progress.total_travelled_distance += current_route.distance
	if current_route.has_cargo:
		Progress.total_deliveries_made += 1
	
	var summaryScreen = RouteSummary.instantiate()
	summaryScreen.route = current_route
	summaryScreen.score = Progress.total_score
	summaryScreen.continued.connect(_summary_closed_after_finished.bind(summaryScreen))
	
	SceneTransition.transition_to_node(actionScreen, summaryScreen)


func _died_on_route(rest_hp: int, passed_km: float, actionScreen):	
	current_hp = rest_hp
	Progress.total_score += current_route.score_when_died(passed_km)
	Progress.total_travelled_distance += passed_km
	
	var summaryScreen = RouteSummary.instantiate()
	summaryScreen.route = current_route
	summaryScreen.score = Progress.total_score
	summaryScreen.died = true
	summaryScreen.continued.connect(_summary_closed_after_died.bind(summaryScreen))
	
	SceneTransition.transition_to_node(actionScreen, summaryScreen)
	
func _puked_on_route(rest_hp: int, passed_km: float, actionScreen):
	
	current_hp = rest_hp
	Progress.total_score += current_route.score_when_puked(passed_km)
	Progress.total_travelled_distance += passed_km
	
	var summaryScreen = RouteSummary.instantiate()
	summaryScreen.route = current_route
	summaryScreen.score = Progress.total_score
	summaryScreen.puked = true
	summaryScreen.continued.connect(_summary_closed_after_puked.bind(summaryScreen))
	
	SceneTransition.transition_to_node(actionScreen, summaryScreen)


func _summary_closed_after_finished(summaryScreen):
	if len(_routes_to_fly) == 0:
		Progress.days_passed += 1
		SceneTransition.transition_to_file(summaryScreen, "res://scenes/map/Map.tscn")
	else:
		summaryScreen.queue_free()
		loadNextRoute()
	

func _summary_closed_after_puked(summaryScreen):
	if len(_routes_to_fly) == 0:
		Progress.days_passed += 1
		SceneTransition.transition_to_file(summaryScreen, "res://scenes/map/Map.tscn")
	else:
		summaryScreen.queue_free()
		loadNextRoute()
	

func _summary_closed_after_died(summaryScreen):
		SceneTransition.transition_to_file(summaryScreen, "res://scenes/GameOver.tscn")

