extends Control

func _ready():
	MusicPlayer.play_menu()
	$AnimationPlayer.play("title")

func _on_start_button_pressed():
	UiSounds.play_click_sound()
	Progress.reset()
	SceneTransition.transition_to_file(self, "res://scenes/map/Map.tscn")


func _on_exit_button_pressed():
	UiSounds.play_click_sound()
	get_tree().quit()


func _on_button_mouse_entered():
	UiSounds.play_hover_sound()

