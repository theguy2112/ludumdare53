extends Node

var total_score = 0
var total_travelled_distance = 0
var total_deliveries_made = 0
var days_passed = 0


func reset():
	total_score = 0
	total_travelled_distance = 0
	total_deliveries_made = 0
	days_passed = 0
