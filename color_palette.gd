extends Node

var colors = [
	Color("85daeb"),
	Color("5fc9e7"),
	Color("5fa1e7"),
	Color("5f6ee7"),
	Color("4c60aa"),
	Color("444774"),
	Color("32313b"),
	Color("463c5e"),
	Color("5d4776"),
	Color("855395"),
	Color("ab58a8"),
	Color("ca60ae"),
	Color("f3a787"),
	Color("f5daa7"),
	Color("8dd894"),
	Color("5dc190"),
	Color("4ab9a3"),
	Color("4593a5"),
	Color("5efdf7"),
	Color("ff5dcc"),
	Color("fdfe89"),
	Color("ffffff"),
]

func get_random_planet_color(rng: RandomNumberGenerator = null) -> Color:
	if rng == null:
		return planet_colors.pick_random()
	else:
		return planet_colors[rng.randi_range(0, len(planet_colors)-1)]
	

var planet_colors = colors.slice(0,-4);
