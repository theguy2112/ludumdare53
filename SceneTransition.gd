extends Control

var _next_scene: Node
var _next_scene_file: String
var _prev_scene: Node
var _transitioning_to_file: bool = false

func _ready():
	$AnimationPlayer.playback_speed = 0.6

func transition_to_file(prev_scene, next_scene_file):
	_next_scene_file = next_scene_file
	_prev_scene = prev_scene
	_transitioning_to_file = true
	$AnimationPlayer.play("blend_in")


func transition_to_node(prev_scene, next_scene):
	_next_scene = next_scene
	_prev_scene = prev_scene
	_transitioning_to_file = false
	$AnimationPlayer.play("blend_in")


func _on_animation_player_animation_finished(anim_name):
	if anim_name == "blend_in":
		if _transitioning_to_file:
			get_tree().change_scene_to_file(_next_scene_file)
		else:
			get_tree().root.add_child(_next_scene)
		_prev_scene.queue_free()
		$AnimationPlayer.play("blend_out")
	elif anim_name == "blend_out":
		visible = false
