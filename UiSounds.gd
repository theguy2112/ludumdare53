extends Node2D

func play_hover_sound():
	$HoverSoundPlayer.play()

func play_click_sound():
	$ClickSoundPlayer.play()
