extends Resource
class_name Route

var from_planet_seed: int
var to_planet_seed: int
var distance: float
var has_cargo: bool

func _init(from_planet_seed: int, to_planet_seed: int, distance: float, has_cargo: bool):
	self.from_planet_seed = from_planet_seed
	self.to_planet_seed = to_planet_seed
	self.distance = distance
	self.has_cargo = has_cargo

func score_when_finished():
	var score = distance
	if has_cargo:
		score *= 1.5
	return score * 50

func score_when_puked(passed_distance: float):
	var score = passed_distance
	if has_cargo:
		score *= 0.8
	return score * 50

func score_when_died(passed_distance: float):
	var score = passed_distance
	if has_cargo:
		score *= 0.5
	return score * 50
